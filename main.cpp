/*������� 2. �������� �������������� �����

��� ����� �������

������������� �������� ������� ��������� �����: ����,
�������, �������������� �����������, �������������
��������������.

��� ���� ���� ����� ���� ����� ����-������ � ���
���������� �� ������ � �������� ����.

��� ��������� ����� ���� � ���������� ���������: ������
� ��� �����, ����� ����� � ��� �������� �
��������������� ������������. ��� ������ � ���
������������ �����.

��� ������ �� ����� ��������� ���������� ����� ����������
������� ������, � ����� ����� ���������� �����������
� ������������ ����� ���� ������, �� ���� �����
��������������, ����������� ������ ���������.

��� ����� ������������� ���� ������������� ������: circle,
square, triangle, rectangle, ��������������� �������.
������� ������ �������� ��������� ����� � �������� ��
������� � ����������� ��������������.

������ � ������������

��� ����� ������ �� ������ ������������ ����������� ��� enum.*/

#include <iostream>
#include "Geometry.h"

void Output(Figure* fig) {
    std::cout << "Area = " << fig->Area() << "\n";
    std::cout << "Minimum & maximum figure points:\n";
    std::cout << "(" << fig->MinAndMaxFigPoint().maxPoint.x << ";";
    std::cout << fig->MinAndMaxFigPoint().maxPoint.y << ")\n";
    std::cout << "(" << fig->MinAndMaxFigPoint().minPoint.x << ";";
    std::cout << fig->MinAndMaxFigPoint().minPoint.y << ")\n";
}

int main()
{
    std::cout << "Please input command\n";
    std::string cmd;
    std::cin >> cmd;

    if (cmd == "circle") {
        Circle* circle = new Circle;
        Coord temp{ 3,3 };
        circle->SetCoordinates(temp);
        circle->SetRadius(3);
        Output(circle);

    }
    else if (cmd == "square") {
        Square* square = new Square;
        Coord temp{ 3,3 };
        square->SetCoordinates(temp);
        square->SetEdgeLength(3);
        Output(square);
    }
    else if (cmd == "triangle") {
        Triangle* triangle = new Triangle;
        Coord temp{ 3,3 };
        triangle->SetCoordinates(temp);
        triangle->SetEdgeLength(10);
        triangle->SetBase(3);
        Output(triangle);
    }
    else if (cmd == "rectangle") {
        Rectangle* rectangle = new Rectangle;
        Coord temp{ 3,3 };
        rectangle->SetCoordinates(temp);
        rectangle->SetWidth(5);
        rectangle->SetHeight(10);
        Output(rectangle);
    }
}



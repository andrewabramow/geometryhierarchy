#include "Geometry.h"

const float  PI_F = 3.14159265358979f;

void Figure::SetCoordinates(const Coord& coord) {
	coordinates = coord;
}
void Figure::SetColour(const Colour& col) {
	colour = col;
}
const float& Figure::Area()const {
	std::cerr << "Area method must be overridden!\n";
	return 0;
}
const TwoCoord& Figure::MinAndMaxFigPoint() const {
	std::cerr << "MinAndMaxFigPoint method must be overridden!\n";
	TwoCoord temp;
	return temp;
}
const Coord& Figure::GetCoordinates() const {
	return coordinates;
}
const Colour& Figure::GetColour() const {
	return colour;
}
//___________Circle______________
void Circle::SetRadius(const float& r) {
	radius = r;
}
const float& Circle::Area()const {
	return PI_F * pow(radius, 2);
}
const TwoCoord& Circle::MinAndMaxFigPoint() const {
	TwoCoord temp;
	temp.maxPoint.x = GetCoordinates().x + radius;
	temp.maxPoint.y = GetCoordinates().y + radius;
	temp.minPoint.x = GetCoordinates().x - radius;
	temp.minPoint.y = GetCoordinates().y - radius;
	return temp;
}
//___________Square______________
void Square::SetEdgeLength(const float& eL) {
	edgeLength = eL;
}
const float& Square::GetEdgeLength() const {
	return edgeLength;
}
const float& Square::Area()const {
	return pow(edgeLength, 2);
}
const TwoCoord& Square::MinAndMaxFigPoint() const {
	TwoCoord temp;
	temp.maxPoint.x = GetCoordinates().x + edgeLength / 2;
	temp.maxPoint.y = GetCoordinates().y + edgeLength / 2;
	temp.minPoint.x = GetCoordinates().x - edgeLength / 2;
	temp.minPoint.y = GetCoordinates().y - edgeLength / 2;
	return temp;
}
//___________Triangle______________
void Triangle::SetBase(const float& b) {
	base = b;
}
const float& Triangle::Area()const {
	return (base * pow((pow(GetEdgeLength(),2)-(pow(base,2)/4)),0.5) / 2);
}
const TwoCoord& Triangle::MinAndMaxFigPoint() const {
	TwoCoord temp;
	float h = pow((pow(GetEdgeLength(), 2) - (pow(base, 2) / 4)), 0.5);
	temp.maxPoint.x = GetCoordinates().x + base / 2;
	temp.maxPoint.y = GetCoordinates().y + h / 2;
	temp.minPoint.x = GetCoordinates().x - base / 2;
	temp.minPoint.y = GetCoordinates().y - h / 2;
	return temp;
}
//___________Rectangle______________

void Rectangle::SetWidth(const float& w) {
	width = w;
}
const float& Rectangle::GetWidth() const {
	return width;
}
void Rectangle::SetHeight(const float& h) {
	height = h;
}
const float& Rectangle::GetHeight() const {
	return height;
}
const float& Rectangle::Area()const {
	return (4 * height * width)  // side surfaces
		+ (2 * pow(width, 2));  // top & bottom surfaces
}
const TwoCoord& Rectangle::MinAndMaxFigPoint() const {
	TwoCoord temp;  // XY projection
	temp.maxPoint.x = GetCoordinates().x + width / 2;
	temp.maxPoint.y = GetCoordinates().y + height / 2;
	temp.minPoint.x = GetCoordinates().x - width / 2;
	temp.minPoint.y = GetCoordinates().y - height / 2;
	return temp;
}
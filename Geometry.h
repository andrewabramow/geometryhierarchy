#pragma once
#include <iostream>

enum Colour {
	RED,
	BLACK,
	WHITE,
	GREEN
};

struct Coord {
	float x = .0f;
	float y = .0f;
};

struct TwoCoord {
	Coord minPoint;
	Coord maxPoint;
};

class Figure {
private:
	Coord coordinates;
	Colour colour;
public:
	Figure() : coordinates{ 0,0 }, colour{ WHITE } {}
	virtual ~Figure() {
		std::cout << "Figure deleted\n";
	}
	virtual const Coord& GetCoordinates() const;
	virtual const Colour& GetColour() const;
	virtual void SetCoordinates(const Coord& coordinates);
	virtual void SetColour(const Colour& colour);
	virtual const float& Area()const;
	virtual const TwoCoord& MinAndMaxFigPoint() const;
};

class Circle : public Figure {
private:
	float radius;
public:
	Circle(): radius(10.0f) {}
	Circle(float inRadius) : radius(inRadius) {}
	~Circle() {
	std::cout << "Circle deleted\n";
	}
	void SetRadius(const float& r);
	const float& Area()const override;
	const TwoCoord& MinAndMaxFigPoint() const override;
};

class Square : public Figure {
private:
	float edgeLength;
public:
	Square(): edgeLength(10.0f){}
	Square(float inEdgeLength) : edgeLength(inEdgeLength) {}
	~Square() {
		std::cout << "Square deleted\n";
	}
	void SetEdgeLength(const float& eL);
	const float& GetEdgeLength() const;
	virtual const float& Area()const override;
	virtual const TwoCoord& MinAndMaxFigPoint() const override;
};

class Triangle : public Square {
private:
	float base;
public:
	Triangle():base(5.0f) {}
	Triangle(float inBase, float inEdgeLength) :base(inBase){
		SetEdgeLength(inEdgeLength);
	}
	~Triangle() {
		std::cout << "Triangle deleted\n";
	}
	void SetBase(const float& b);
	const float& Area()const override;
	const TwoCoord& MinAndMaxFigPoint() const override;
};

class Rectangle : public Figure {
private:
	float width;
	float height;
public:
	Rectangle(): width(5.0f), height(10.0f){}
	Rectangle(float inWidth, float inHeight) :
		width(inWidth), height(inHeight) {}
	~Rectangle() {}
	void SetWidth(const float& w);
	const float& GetWidth() const;
	void SetHeight(const float& w);
	const float& GetHeight() const;
	const float& Area()const override;
	const TwoCoord& MinAndMaxFigPoint() const override;
};